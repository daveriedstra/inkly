# InkLy - Basic Lilypond file support for Inkscape

This extension is basically the example [Call Extension](https://gitlab.com/inkscape/extensions/-/wikis/home) modified for Lilypond files. I've had no luck testing it, partially due to [this bug](https://gitlab.com/inkscape/inkscape/-/issues/2611) keeping me on flatpak inkscape for now.

This *should* work fine for single-page outputs. I don't know what will happen if there's more than one page.

## Installation

Theoretically you should be able to move the .inx and .py files to `/usr/share/inkscape/extensions/` and that should be it. (On a flatpak that location is `~/.var/app/org.inkscape.Inkscape/config/inkscape/extensions/` but it didn't work for me -- possibly because the flatpak sandbox kept Inkscape from finding the lilypond binary.)
