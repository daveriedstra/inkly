import inkex
from inkex import command

class InkLy(inkex.CallExtension):
    input_ext = 'ly'
    output_ext = 'svg'

    def call(self, input_file, output_file):
        # lilypond --svg -o $OUTFILE $INFILE
        # What happens when there are multiple pages?
        command.call('lilypond', '--svg', '-o', output_file, input_file)

if __name__ == '__main__':
    InkLy().run()
